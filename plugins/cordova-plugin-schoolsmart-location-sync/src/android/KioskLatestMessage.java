package com.pseudocoders;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by james on 14/09/16.
 */
@IgnoreExtraProperties
public class KioskLatestMessage {

    public static final String TYPE_ETA = "ETA";

    public String type;
    public String time;
    public Coordinates coordinates;

    public KioskLatestMessage() {}
}
