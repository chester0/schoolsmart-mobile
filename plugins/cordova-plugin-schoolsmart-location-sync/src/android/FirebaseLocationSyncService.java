package com.pseudocoders;

import android.Manifest;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DatabaseError;

/**
 * A class representing a background service that listen to a Firebase database
 * for requests for the user's current location.
 */
public class FirebaseLocationSyncService extends IntentService implements LocationListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{

    public static final int LOCATION_TIME_VARIANCE = 60000; // 1 minute

    private int accessCourseLocationResult;
    private int accessFineLocationResult;
    private GoogleApiClient googleApiClient;
    private LocationManager locationManager;
    private Location lastKnownLocation;

    public FirebaseLocationSyncService() {
        super("FirebaseLocationSyncService");
        this.googleApiClient = new GoogleApiClient.Builder(FirebaseLocationSyncService.this).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).
                addApi(LocationServices.API).
                build();
            this.googleApiClient.connect();
    }

    @Override
    protected void onHandleIntent(Intent workIntent) {
        this.start();
    }

    // Starts listening for database changes and responds with the user's current
    // location, if available
    // Should use an internal unique
    private void start() {
        final String guardianId = "exampleGuardianUser"; // store this on device
        final String deviceId = "device1"; // store this on device too
        final DatabaseReference guardianReference = FirebaseDatabase.
        getInstance().
        getReference().
        child("users").
        child(guardianId);

    // Should use an internal unique id to decide which guardian to listen to,
    // for now just hardcode
    ValueEventListener guardianListener = new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
          Guardian guardian = dataSnapshot.getValue(Guardian.class);
          if (guardian.latestMessage.type.equals(GuardianLatestMessage.TYPE_PICKUP) &&
                  guardian.primaryDeviceId.equals(deviceId)) {
              // Update the kiosk with a new eta
              final DatabaseReference kioskReference = FirebaseDatabase.
                      getInstance().
                      getReference().
                      child("kiosks").
                      child(guardian.latestMessage.kioskId);

              kioskReference.addListenerForSingleValueEvent(new ValueEventListener() {

                  private GoogleApiClient googleApiClient;

                  public ValueEventListener initialise(GoogleApiClient googleApiClient) {
                      this.googleApiClient = googleApiClient;
                      return this;
                  }

                  @Override
                  public void onDataChange(DataSnapshot dataSnapshot) {
                      if (this.googleApiClient.hasConnectedApi(LocationServices.API)) {
                          Location lastKnownLocation = this.getLastKnownLocation();

                          if(lastKnownLocation != null) {

                              Kiosk kiosk = dataSnapshot.getValue(Kiosk.class);
                              kiosk.coordinates = dataSnapshot.child("coordinates").getValue(Coordinates.class);

                              // Send a message to the target kiosk
                              KioskLatestMessage newKioskMessage = new KioskLatestMessage();
                              newKioskMessage.type = KioskLatestMessage.TYPE_ETA;
                              newKioskMessage.time = String.valueOf(System.currentTimeMillis());
                              kioskReference.child("latestMessage").setValue(newKioskMessage);

                              // Mark the guardian message we just attended to as old
                              guardianReference.child("latestMessage").child("status").setValue("old");
                          }
                      }
                  }

                  public Location getLastKnownLocation() {
                      Location lastKnownLocation = null;
                      try {
                          lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(this.googleApiClient);
                      } catch (SecurityException e) {
                          System.out.println(e.getMessage());
                      }

                      return lastKnownLocation;
                  }

                  @Override
                  public void onCancelled(DatabaseError databaseError) {}
              });
          }
      }

        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };
    guardianReference.addValueEventListener(guardianListener);
  }

    @Override
    public void onLocationChanged(Location location) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onConnected(@Nullable Bundle bundle) {}

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
