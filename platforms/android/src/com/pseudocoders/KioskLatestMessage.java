package com.pseudocoders;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by james on 14/09/16.
 */
@IgnoreExtraProperties
public class KioskLatestMessage {

    public static final String TYPE_ETA = "ETA";
    public static final String TYPE_LOCATION = "LOCATION";
    public static final String STATUS_NEW = "NEW";

    public String type;
    public String status;
    public Coordinates coordinates;

    public KioskLatestMessage() {}
}
