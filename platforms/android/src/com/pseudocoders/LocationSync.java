package com.pseudocoders;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.pseudocoders.FirebaseLocationSyncService;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
/**
 * Represents the locationSync plugin, which starts a background service to
 * listen to a Firebase database for requests for the user's current location.
 */

public class LocationSync extends CordovaPlugin implements ActivityCompat.OnRequestPermissionsResultCallback{

    public static final int LOCATION_PERMISSION_REQUEST = 1;
    public static final int GUARDIAN_ID_INDEX = 0;

    private String guardianId;

    @Override

    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        this.guardianId = data.getString(LocationSync.GUARDIAN_ID_INDEX);

        boolean haveLocationPermissions = (ContextCompat.checkSelfPermission(
                cordova.getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) &&
            (ContextCompat.checkSelfPermission(
                    cordova.getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);

        // Ask for permission to get the user's location, if it is already granted, start the service
        if (!haveLocationPermissions) {
            ActivityCompat.requestPermissions(
                    this.cordova.getActivity(),
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    },
                    LocationSync.LOCATION_PERMISSION_REQUEST
            );
        } else {
            // We have permission so start the service
            this.start();
        }
        return true;
    }

    @Override
    // Callback invoked after the user has either granted or denied permission for location.
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LocationSync.LOCATION_PERMISSION_REQUEST: {
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // We got permission, start the location service
                    this.start();
                } else {
                    // Didnt get permission
                    System.out.println("Failed to get user location permission");
                }
                break;
            }
        }
        this.start();
    }

    // Starts the background service. The service will not be started if guardianId is unset
    public void start() {
        if (this.guardianId != null) {
            Intent startIntent = new Intent(cordova.getActivity(), FirebaseLocationSyncService.class);
            startIntent.putExtra("guardianId", this.guardianId);
            cordova.getActivity().startService(startIntent);
        }
    }
}
