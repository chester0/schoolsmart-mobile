package com.pseudocoders;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by james on 14/09/16.
 */
@IgnoreExtraProperties
public class Kiosk {

    public Coordinates coordinates;
    public KioskLatestMessage latestMessage;

    public Kiosk(){}
}
