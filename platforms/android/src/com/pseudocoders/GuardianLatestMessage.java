package com.pseudocoders;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by james on 14/09/16.
 */
@IgnoreExtraProperties
public class GuardianLatestMessage {

    public static final String TYPE_PICKUP = "pickupMessage";
    public static final String STATUS_NEW = "NEW";
    public static final String STATUS_OLD = "OLD";

    public String kioskId;
    public String status;
    public String type;

    public GuardianLatestMessage(){}
}
