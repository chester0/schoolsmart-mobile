materialAdmin
    // =========================================================================
    // Base controller for common functions
    // =========================================================================

    .controller('materialadminCtrl', function($timeout, $state, $scope, growlService){
        //Welcome Message
        //growlService.growl('Welcome back Mallinda!', 'inverse')

        // Detact Mobile Browser
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           angular.element('html').addClass('ismobile');
        }

        // By default Sidbars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
        this.sidebarToggle = {
            left: false,
            right: false
        }

        // By default template has a boxed layout
        this.layoutType = localStorage.getItem('ma-layout-status');

        // For Mainmenu Active Class
        this.$state = $state;

        //Close sidebar on click
        this.sidebarStat = function(event) {
            if (!angular.element(event.target).parent().hasClass('active')) {
                this.sidebarToggle.left = false;
            }
        }

        //Listview Search (Check listview pages)
        this.listviewSearchStat = false;

        this.lvSearch = function() {
            this.listviewSearchStat = true;
        }

        //Listview menu toggle in small screens
        this.lvMenuStat = false;

        //Blog
        this.wallCommenting = [];

        this.wallImage = false;
        this.wallVideo = false;
        this.wallLink = false;

        //Skin Switch
        this.currentSkin = 'blue';

        this.skinList = [
            'lightblue',
            'bluegray',
            'cyan',
            'teal',
            'green',
            'orange',
            'blue',
            'purple'
        ]

        this.skinSwitch = function (color) {
            this.currentSkin = color;
        }

        $scope.user;
        $scope.isAdmin = false;

        //User Auth State Monitor
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                // User is signed in.
                $scope.user = user;
                //Check and record if user is admin
                var userRef = firebase.database().ref('users/' + user.uid)
                userRef.child("/roles/").once("value").then(function(rolesSnapshot) {
                    $scope.$apply(function(){
                        $scope.isAdmin = rolesSnapshot.hasChild("admin");
                    });
                });

                // Attempt to start the background service, if available
                if (typeof(locationSync) != "undefined") {
                  locationSync.start(user.uid, function(message){
                    alert(message);
                  }, function() {
                    alert(message);
                  });
                }
                // TODO: send the user to the page they were on instead of home
                $state.go("home");
            } else {
                // No user is signed in.
                $state.go("login");
            }
        })

        //User Logout
        this.logOut = function () {
            firebase.auth().signOut().then(function() {
                // Sign-out successful.
            }, function(error) {
                // An error happened.
                alert("User could not be signed out!");
            });
        };

    })


    // =========================================================================
    // Header
    // =========================================================================
    .controller('headerCtrl', function($timeout, messageService){


        // Top Search
        this.openSearch = function(){
            angular.element('#header').addClass('search-toggled');
            angular.element('#top-search-wrap').find('input').focus();
        }

        this.closeSearch = function(){
            angular.element('#header').removeClass('search-toggled');
        }

        // Get messages and notification for header
        this.img = messageService.img;
        this.user = messageService.user;
        this.user = messageService.text;

        this.messageResult = messageService.getMessage(this.img, this.user, this.text);


        //Clear Notification
        this.clearNotification = function($event) {
            $event.preventDefault();

            var x = angular.element($event.target).closest('.listview');
            var y = x.find('.lv-item');
            var z = y.size();

            angular.element($event.target).parent().fadeOut();

            x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
            x.find('.grid-loading').fadeIn(1500);
            var w = 0;

            y.each(function(){
                var z = $(this);
                $timeout(function(){
                    z.addClass('animated fadeOutRightBig').delay(1000).queue(function(){
                        z.remove();
                    });
                }, w+=150);
            })

            $timeout(function(){
                angular.element('#notifications').addClass('empty');
            }, (z*150)+200);
        }

        // Clear Local Storage
        this.clearLocalStorage = function() {

            //Get confirmation, if confirmed clear the localStorage
            swal({
                title: "Are you sure?",
                text: "All your saved localStorage values will be removed",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function(){
                localStorage.clear();
                swal("Done!", "localStorage is cleared", "success");
            });

        }

        //Fullscreen View
        this.fullScreen = function() {
            //Launch
            function launchIntoFullscreen(element) {
                if(element.requestFullscreen) {
                    element.requestFullscreen();
                } else if(element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if(element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if(element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }

            //Exit
            function exitFullscreen() {
                if(document.exitFullscreen) {
                    document.exitFullscreen();
                } else if(document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if(document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }

            if (exitFullscreen()) {
                launchIntoFullscreen(document.documentElement);
            }
            else {
                launchIntoFullscreen(document.documentElement);
            }
        }

    })



    // =========================================================================
    // Best Selling Widget
    // =========================================================================

    .controller('bestsellingCtrl', function(bestsellingService){
        // Get Best Selling widget Data
        this.img = bestsellingService.img;
        this.name = bestsellingService.name;
        this.range = bestsellingService.range;

        this.bsResult = bestsellingService.getBestselling(this.img, this.name, this.range);
    })


    // =========================================================================
    // Todo List Widget
    // =========================================================================

    .controller('todoCtrl', function(todoService){

        //Get Todo List Widget Data
        this.todo = todoService.todo;

        this.tdResult = todoService.getTodo(this.todo);

        //Add new Item (closed by default)
        this.addTodoStat = false;
    })


    // =========================================================================
    // Recent Items Widget
    // =========================================================================

    .controller('recentitemCtrl', function(recentitemService){

        //Get Recent Items Widget Data
        this.id = recentitemService.id;
        this.name = recentitemService.name;
        this.parseInt = recentitemService.price;

        this.riResult = recentitemService.getRecentitem(this.id, this.name, this.price);
    })


    // =========================================================================
    // Recent Posts Widget
    // =========================================================================

    .controller('recentpostCtrl', function(recentpostService){

        //Get Recent Posts Widget Items
        this.img = recentpostService.img;
        this.user = recentpostService.user;
        this.text = recentpostService.text;

        this.rpResult = recentpostService.getRecentpost(this.img, this.user, this.text);
    })


    //=================================================
    // Profile
    //=================================================

    .controller('profileCtrl', function(growlService){

        //Get Profile Information from profileService Service

        //User
        this.profileSummary = "Sed eu est vulputate, fringilla ligula ac, maximus arcu. Donec sed felis vel magna mattis ornare ut non turpis. Sed id arcu elit. Sed nec sagittis tortor. Mauris ante urna, ornare sit amet mollis eu, aliquet ac ligula. Nullam dolor metus, suscipit ac imperdiet nec, consectetur sed ex. Sed cursus porttitor leo.";

        this.fullName = "Student1";
        this.gender = "female";
        this.birthDay = "23/06/1988";
        this.martialStatus = "Single";
        this.mobileNumber = "00971123456789";
        this.emailAddress = "test@gmail.com";
        this.twitter = "@test";
        this.twitterUrl = "twitter.com/test";
        this.skype = "malinda.hollaway";
        this.addressSuite = "44-46 Morningside Road";
        this.addressCity = "Edinburgh";
        this.addressCountry = "Scotland";

        //Edit
        this.editSummary = 0;
        this.editInfo = 0;
        this.editContact = 0;


        this.submit = function(item, message) {
            if(item === 'profileSummary') {
                this.editSummary = 0;
            }

            if(item === 'profileInfo') {
                this.editInfo = 0;
            }

            if(item === 'profileContact') {
                this.editContact = 0;
            }

            growlService.growl(message+' has updated Successfully!', 'inverse');
        }

    })



    //=================================================
    // LOGIN
    //=================================================

    .controller('loginCtrl', function($scope, $state, $window, $firebaseArray){

        //Status
        this.login = 1;
        this.register = 0;
        this.forgot = 0;

        //Empty form data for registering and login
        //Apparently Angular only likes reading values from the view if they're object properties, so...
        $scope.register = {};
        $scope.login = {};
        $scope.support = {};

        //Populating Register School list selector
        //See login.html, uses ng-options
        //Defining referance values for schools
        var schoolsRef = firebase.database().ref("/schools/");
        schoolsRef.once("value").then(function(schoolListSnapshot) {
            $scope.schoolList = schoolListSnapshot.val();
        });

        //New Account Creation
        $scope.registerAccount = function (){
            if ($scope.register.username != null &&
                $scope.register.emailAddress != null &&
                $scope.register.password != null &&
                $scope.register.school != null){
                firebase.auth().createUserWithEmailAndPassword($scope.register.emailAddress, $scope.register.password)
                    .then(function(firebaseUser) {
                    firebaseUser.updateProfile({
                        displayName: $scope.register.username
                    }).then(function() {
                        // Update successful
                        var userRef = firebase.database().ref('users/' + firebaseUser.uid);

                        //Set users name in the non-auth database
                        userRef.set({
                            name: $scope.register.username
                        });

                        //Set users associated school
                        var schoolData = {};
                        schoolData[$scope.register.school] = true;
                        userRef.child("schools").set(schoolData);

                        //Set users associated role
                        var roleData = {};
                        roleData["guardian"] = true;
                        userRef.child("roles").set(roleData);

                    }, function(error) {
                        // An error happened.
                        alert("Error: " + error.code + error.message);
                    });
                }).catch (function(error) {
                    // Handle Errors here.
                    alert("Error: " + error.code + error.message);
                });
            }
            else {
                alert("Error: Please enter your information into all fields to continue.");
            }
        }

        //Email Sign In
        $scope.emailSignIn = function (){
            if ($scope.login.emailAddress != null &&
                $scope.login.password != null){
                firebase.auth().signInWithEmailAndPassword($scope.login.emailAddress, $scope.login.password)
                    .then(function(firebaseUser) {
                    //Put "Register Login with AuthService" code here
                    $state.go('home');
                }).catch(function(error) {
                    alert("Error: \n" + error.code + error.message);
                });
            }
            else {
                alert("Error: Please enter an email address and password to continue, or click the red button below to register an account.");
            }
        }

        //Google Sign In
        $scope.googleSignIn = function () {
            var provider = new firebase.auth.GoogleAuthProvider();
            firebase.auth().signInWithRedirect(provider);
        };

        //Password Reset Email
        $scope.passwordReset = function () {
            firebase.auth().sendPasswordResetEmail($scope.support.emailAddress)
                .then(function() {
                // Email sent.
                alert("An email has been sent to the provided address, and should arrive shortly. Please follow the contained instructions to reset your password.")
            }, function(error) {
                // An error happened.
                alert("Error: \n" + error.code + error.message);
            });
        }



    })


    //=================================================
    // CALENDAR
    //=================================================

    .controller('calendarCtrl', function($scope, $modal){

        //Get list of students associated with the user
        var userStudentsListRef = firebase.database().ref("/users/" + $scope.user.uid + "/students/");
        userStudentsListRef.on("value", function(studentListSynced) {
            $scope.studentList = [];
            studentListSynced.forEach(function(studentInList) {
                var studentKey = studentInList.key;
                var studentRef = firebase.database().ref("/users/" + studentKey);
                studentRef.once("value").then(function (studentInfo) {
                    $scope.$apply(function(){
                        $scope.studentList.push(studentInfo.val());
                    });
                });
            });
        });

        //Create and add Action button with dropdown in Calendar header.
        this.month = 'month';

        //Open new event modal on selecting a day
        this.onSelect = function(argStart, argEnd) {
            var modalInstance  = $modal.open({
                templateUrl: 'addEvent.html',
                controller: 'addeventCtrl',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    calendarData: function() {
                        var x = [argStart, argEnd];
                        return x;
                    }
                }
            });
        }
    })

    //Add event Controller (Modal Instance)
    .controller('addeventCtrl', function($scope, $modalInstance, calendarData){

        //Calendar Event Data
        $scope.calendarData = {
            eventStartDate: calendarData[0],
            eventEndDate:  calendarData[1]
        };

        //Tags
        $scope.tags = [
            'bgm-teal',
            'bgm-red',
            'bgm-pink',
            'bgm-blue',
            'bgm-lime',
            'bgm-green',
            'bgm-cyan',
            'bgm-orange',
            'bgm-purple',
            'bgm-gray',
            'bgm-black',
        ]

        //Select Tag
        $scope.currentTag = '';

        $scope.onTagClick = function(tag, $index) {
            $scope.activeState = $index;
            $scope.activeTagColor = tag;
        }

        //Add new event
        $scope.addEvent = function() {
            if ($scope.calendarData.eventName) {

                //Render Event
                $('#calendar').fullCalendar('renderEvent',{
                    title: $scope.calendarData.eventName,
                    start: $scope.calendarData.eventStartDate,
                    end:  $scope.calendarData.eventEndDate,
                    allDay: true,
                    className: $scope.activeTagColor

                },true ); //Stick the event

                $scope.activeState = -1;
                $scope.calendarData.eventName = '';
                $modalInstance.close();
            }
        }

        //Dismiss
        $scope.eventDismiss = function() {
            $modalInstance.dismiss();
        }
    })

    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('formCtrl', function(){

        //Input Slider
        this.nouisliderValue = 4;
        this.nouisliderFrom = 25;
        this.nouisliderTo = 80;
        this.nouisliderRed = 35;
        this.nouisliderBlue = 90;
        this.nouisliderCyan = 20;
        this.nouisliderAmber = 60;
        this.nouisliderGreen = 75;

        //Color Picker
        this.color = '#03A9F4';
        this.color2 = '#8BC34A';
        this.color3 = '#F44336';
        this.color4 = '#FFC107';
    })


    // =========================================================================
    // PHOTO GALLERY
    // =========================================================================

    .controller('photoCtrl', function(){

        //Default grid size (2)
        this.photoColumn = 'col-md-2';
        this.photoColumnSize = 2;

        this.photoOptions = [
            { value: 2, column: 6 },
            { value: 3, column: 4 },
            { value: 4, column: 3 },
            { value: 1, column: 12 },
        ]

        //Change grid
        this.photoGrid = function(size) {
            this.photoColumn = 'col-md-'+size;
            this.photoColumnSize = size;
        }

    })


    // =========================================================================
    // ANIMATIONS DEMO
    // =========================================================================
    .controller('animCtrl', function($timeout){

        //Animation List
        this.attentionSeekers = [
            { animation: 'bounce', target: 'attentionSeeker' },
            { animation: 'flash', target: 'attentionSeeker' },
            { animation: 'pulse', target: 'attentionSeeker' },
            { animation: 'rubberBand', target: 'attentionSeeker' },
            { animation: 'shake', target: 'attentionSeeker' },
            { animation: 'swing', target: 'attentionSeeker' },
            { animation: 'tada', target: 'attentionSeeker' },
            { animation: 'wobble', target: 'attentionSeeker' }
        ]
        this.flippers = [
            { animation: 'flip', target: 'flippers' },
            { animation: 'flipInX', target: 'flippers' },
            { animation: 'flipInY', target: 'flippers' },
            { animation: 'flipOutX', target: 'flippers' },
            { animation: 'flipOutY', target: 'flippers'  }
        ]
         this.lightSpeed = [
            { animation: 'lightSpeedIn', target: 'lightSpeed' },
            { animation: 'lightSpeedOut', target: 'lightSpeed' }
        ]
        this.special = [
            { animation: 'hinge', target: 'special' },
            { animation: 'rollIn', target: 'special' },
            { animation: 'rollOut', target: 'special' }
        ]
        this.bouncingEntrance = [
            { animation: 'bounceIn', target: 'bouncingEntrance' },
            { animation: 'bounceInDown', target: 'bouncingEntrance' },
            { animation: 'bounceInLeft', target: 'bouncingEntrance' },
            { animation: 'bounceInRight', target: 'bouncingEntrance' },
            { animation: 'bounceInUp', target: 'bouncingEntrance'  }
        ]
        this.bouncingExits = [
            { animation: 'bounceOut', target: 'bouncingExits' },
            { animation: 'bounceOutDown', target: 'bouncingExits' },
            { animation: 'bounceOutLeft', target: 'bouncingExits' },
            { animation: 'bounceOutRight', target: 'bouncingExits' },
            { animation: 'bounceOutUp', target: 'bouncingExits'  }
        ]
        this.rotatingEntrances = [
            { animation: 'rotateIn', target: 'rotatingEntrances' },
            { animation: 'rotateInDownLeft', target: 'rotatingEntrances' },
            { animation: 'rotateInDownRight', target: 'rotatingEntrances' },
            { animation: 'rotateInUpLeft', target: 'rotatingEntrances' },
            { animation: 'rotateInUpRight', target: 'rotatingEntrances'  }
        ]
        this.rotatingExits = [
            { animation: 'rotateOut', target: 'rotatingExits' },
            { animation: 'rotateOutDownLeft', target: 'rotatingExits' },
            { animation: 'rotateOutDownRight', target: 'rotatingExits' },
            { animation: 'rotateOutUpLeft', target: 'rotatingExits' },
            { animation: 'rotateOutUpRight', target: 'rotatingExits'  }
        ]
        this.fadeingEntrances = [
            { animation: 'fadeIn', target: 'fadeingEntrances' },
            { animation: 'fadeInDown', target: 'fadeingEntrances' },
            { animation: 'fadeInDownBig', target: 'fadeingEntrances' },
            { animation: 'fadeInLeft', target: 'fadeingEntrances' },
            { animation: 'fadeInLeftBig', target: 'fadeingEntrances'  },
            { animation: 'fadeInRight', target: 'fadeingEntrances'  },
            { animation: 'fadeInRightBig', target: 'fadeingEntrances'  },
            { animation: 'fadeInUp', target: 'fadeingEntrances'  },
            { animation: 'fadeInBig', target: 'fadeingEntrances'  }
        ]
        this.fadeingExits = [
            { animation: 'fadeOut', target: 'fadeingExits' },
            { animation: 'fadeOutDown', target: 'fadeingExits' },
            { animation: 'fadeOutDownBig', target: 'fadeingExits' },
            { animation: 'fadeOutLeft', target: 'fadeingExits' },
            { animation: 'fadeOutLeftBig', target: 'fadeingExits'  },
            { animation: 'fadeOutRight', target: 'fadeingExits'  },
            { animation: 'fadeOutRightBig', target: 'fadeingExits'  },
            { animation: 'fadeOutUp', target: 'fadeingExits'  },
            { animation: 'fadeOutUpBig', target: 'fadeingExits'  }
        ]
        this.zoomEntrances = [
            { animation: 'zoomIn', target: 'zoomEntrances' },
            { animation: 'zoomInDown', target: 'zoomEntrances' },
            { animation: 'zoomInLeft', target: 'zoomEntrances' },
            { animation: 'zoomInRight', target: 'zoomEntrances' },
            { animation: 'zoomInUp', target: 'zoomEntrances'  }
        ]
        this.zoomExits = [
            { animation: 'zoomOut', target: 'zoomExits' },
            { animation: 'zoomOutDown', target: 'zoomExits' },
            { animation: 'zoomOutLeft', target: 'zoomExits' },
            { animation: 'zoomOutRight', target: 'zoomExits' },
            { animation: 'zoomOutUp', target: 'zoomExits'  }
        ]

        //Animate
        this.ca = '';

        this.setAnimation = function(animation, target) {
            if (animation === "hinge") {
                animationDuration = 2100;
            }
            else {
                animationDuration = 1200;
            }

            angular.element('#'+target).addClass(animation);

            $timeout(function(){
                angular.element('#'+target).removeClass(animation);
            }, animationDuration);
        }

    })

    // =========================================================================
    // Admin Account Management Controller
    // =========================================================================

    .controller('accountManagementCtrl', function($scope){

        //Empty form data for registering and login
        //Apparently Angular only likes reading values from the view if they're object properties, so...
        $scope.newStudent = {};

        //Populating School list selector
        $scope.schoolList = [];
        var schoolsRef = firebase.database().ref("/schools/");
        schoolsRef.once("value").then(function(schoolListSnapshot) {
            $scope.schoolList = schoolListSnapshot.val();
        });

        //Populating guardian list selector
        $scope.guardianList = [];
        var usersRef = firebase.database().ref("/users/").orderByChild("roles/guardian").equalTo(true);
        usersRef.once("value").then(function(usersListSnapshot) {
            $scope.guardianList = usersListSnapshot.val();
        });

        $scope.addStudent = function () {
            if ($scope.isAdmin){
                if (confirm('Are you sure you want to add this student to the database?')) {
                    //Determining entry location for new student
                    var newStudentRef = firebase.database().ref("/users/" + $scope.newStudent.rfid);

                    //Set students associated name
                    newStudentRef.set({
                        name: $scope.newStudent.name
                    });

                    //Set students associated school
                    var schoolData = {};
                    schoolData[$scope.newStudent.school] = true;
                    newStudentRef.child("schools").set(schoolData);

                    //Set students associated role
                    var roleData = {};
                    roleData["student"] = true;
                    newStudentRef.child("roles").set(roleData);

                    //Set students associated default guardian
                    var guardianData = {};
                    guardianData["default"] = $scope.newStudent.defGuardian;
                    newStudentRef.child("guardians").set(guardianData);

                    //Set guardians associated student
                    var guardianRef = firebase.database().ref("/users/" + $scope.newStudent.defGuardian);
                    guardianRef.child("students/" + $scope.newStudent.rfid).set(true);

                    alert("Student added!");

                } else {
                    // Do nothing!
                }
            } else {
                alert("You cannot do this unless you are an administrator!")
            }
        };

    });
