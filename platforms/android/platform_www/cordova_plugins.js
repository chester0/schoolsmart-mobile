cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "cordova-plugin-schoolsmart-location-sync.locationSync",
        "file": "plugins/cordova-plugin-schoolsmart-location-sync/www/schoolsmartLocationSync.js",
        "pluginId": "cordova-plugin-schoolsmart-location-sync",
        "clobbers": [
            "locationSync"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.0",
    "cordova-plugin-schoolsmart-location-sync": "1.0.0"
};
// BOTTOM OF METADATA
});