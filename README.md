# Schoolsmart Mobile Repo
This is a combined Cordova plugin and App using the web repository
for the front end

## Requirements
These need to be installed and accessible from the command line:  

* Java SDK
* Android SDK
* Cordova (can be installed with npm install -g cordova)
* Node and npm
* Android Studio

## Setting up
All setup is done using the command line (bash or command prompt).

1. Pull in the code for the web submodule using:  
  git submodule update --init
2. Manually copy the contents of /web/public to the www directory, this would ideally be change to happen automatically when cordova builds the project.
2. Get Cordova to build the application for android using:  
  cordova platform add android --save  
  cordova build
3. For actual development, get npm to pull in required libraries:  
  npm install
To run the latest build inside the emulator, run:  
  cordova emulate android 
  - This assumes that the Android SDK and emulator are setup and working on your machine already.

## Android studio
file open project 
import platforms/android
gradle builds from there (use latest!)

to have the most up to date web repo in the app
copy from web/public* to mobile/platforms/android/assets/www
and rebuild